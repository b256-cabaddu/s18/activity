/* 1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

*/

function sum(a, b) {
    console.log("Displayed sum of " + a + " and " + b)
    console.log(a + b)
}

function minus(a, b) {
    console.log("Displayed difference of " + a + " and " + b)
    console.log(a - b)
}

sum(5, 15)
minus(20, 5)

function multiply(a, b) {
    console.log("The product of " + a + " and " + b)
    return(a * b)
}


let product = multiply(50, 10)
console.log(product)

function divide(a, b) {
    console.log("The quotient of " + a + " and " + b)
    return(a / b)
}

let quotient = divide(50, 10)
console.log(quotient)

function areaCircle(r){
    console.log("The result of getting the area of a circle with " + r + " radius:")
    area = ((3.14159 * (r ** 2)).toFixed(2))
    return(area)
}

let circleArea = areaCircle(15)
console.log(circleArea)


function average(a, b, c, d) {
    console.log("The average of " + a + ", " + b + ", " + c + " and " + d + ":")
    return ((a + b + c + d)/4)
}
let averageVar = average(20, 40, 60, 80)
console.log(averageVar)

function passing(a,b) {
    console.log("Is " + a + "\/" + b + " a passing score?")
    let score = ((a/b)*100)
    let passingScore = score > 75
    return passingScore
}

isPassingScore = passing(38, 50)
console.log(isPassingScore)